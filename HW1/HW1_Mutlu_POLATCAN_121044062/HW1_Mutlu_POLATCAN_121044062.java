import java.util.Scanner;

/**
 * Created by mpolatcan-gyte_cse on 08.04.2017.
 */
public class HW1_Mutlu_POLATCAN_121044062 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        NLPCalculator NLPCalculator = new NLPCalculator();
        NLPCalculator.execute();

        double probability;

        while (true) {
            System.out.print("User Interpolation (Y/N): ");
            String answer = scanner.next();
            System.out.print("N-Gram: ");
            int ngram = scanner.nextInt();
            System.out.print("Enter sentence: ");
            scanner.nextLine();
            String sentence = scanner.nextLine();

            if (answer.equals("Y") || answer.equals("y")) {
                probability = NLPCalculator.assignProbabilityToSentence(sentence, ngram, true);
            } else {
                probability = NLPCalculator.assignProbabilityToSentence(sentence, ngram, false);
            }

            System.out.printf("The probability of the sentence is: %.30f\n\n", probability);
        }
    }
}
