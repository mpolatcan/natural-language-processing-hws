import java.nio.charset.Charset;

/**
 * Created by mpolatcan-gyte_cse on 09.04.2017.
 */
public interface NLPCalculatorConstants {
    String TEST_NEWS_PATH = "test news";
    String NEWS_PATH = "news";
    String ECONOMY = NEWS_PATH + "/economy";
    String HEALTH = NEWS_PATH + "/health";
    String MAGAZINE = NEWS_PATH + "/magazine";
    String POLITICAL = NEWS_PATH + "/political";
    String SPORTS = NEWS_PATH + "/sports";
    String UNIGRAM_RESULT_FILE = "UnigramResults.txt";
    String BIGRAM_RESULT_FILE = "BigramResults.txt";
    String TRIGRAM_RESULT_FILE = "TrigramResults.txt";
    String TETRAGRAM_RESULT_FILE = "TetragramResults.txt";
    String PENTAGRAM_RESULT_FILE = "PentagramResults.txt";
    String PERPLEXITY_RESULTS_FILE = "PerplexityResults.txt";
    Charset TR_CHARSET = Charset.forName("ISO-8859-9");
    int UNIGRAM = 1;
    int BIGRAM = 2;
    int TRIGRAM = 3;
    int TETRAGRAM = 4;
    int PENTAGRAM = 5;
}
