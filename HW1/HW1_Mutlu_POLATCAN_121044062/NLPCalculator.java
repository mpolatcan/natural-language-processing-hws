import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mpolatcan-gyte_cse on 09.04.2017.
 */
public class NLPCalculator {
    // ------------------------ INPUT AND OUTPUT PATHS ------------------------------
    private final String[] trainingSetPaths =
            { NLPCalculatorConstants.ECONOMY, NLPCalculatorConstants.HEALTH,
              NLPCalculatorConstants.MAGAZINE, NLPCalculatorConstants.POLITICAL,
              NLPCalculatorConstants.SPORTS };

    private final String[] gramResultsFilenames =
            { NLPCalculatorConstants.UNIGRAM_RESULT_FILE, NLPCalculatorConstants.BIGRAM_RESULT_FILE,
              NLPCalculatorConstants.TRIGRAM_RESULT_FILE, NLPCalculatorConstants.TETRAGRAM_RESULT_FILE,
              NLPCalculatorConstants.PENTAGRAM_RESULT_FILE };
    // -----------------------------------------------------------------------------

    // --------------------------- N-GRAM COUNTS -----------------------------------
    private Map<String,Integer> unigramCounts;
    private Map<String, Integer> bigramCounts;
    private Map<String, Integer> trigramCounts;
    private Map<String, Integer> tetragramCounts;
    private Map<String, Integer> pentagramCounts;
    // -----------------------------------------------------------------------------

    // ----------------------- N-GRAM PERPLEXITIES ----------------------------------
    private Map<String, Double> unigramPerplexities;
    private Map<String, Double> bigramPerplexities;
    private Map<String, Double> trigramPerplexities;
    private Map<String, Double> tetragramPerplexities;
    private Map<String, Double> pentagramPerplexities;
    private Map<String, Double> interpolatedPerplexities;
    // -------------------------------------------------------------------------------

    // ----------------------- INTERPOLATION LAMBDA VALUES ---------------------------
    private double UNIGRAM_LAMBDA = 0.1;
    private double BIGRAM_LAMBDA = 0.4;
    private double TRIGRAM_LAMBDA = 0.3;
    private double TETRAGRAM_LAMBDA = 0.1;
    private double PENTAGRAM_LAMBDA = 0.1;
    // -------------------------------------------------------------------------------

    private int totalCharacter = 0; // total characters for unigram probability calculation

    public NLPCalculator() {
        unigramCounts = new HashMap<>();
        bigramCounts = new HashMap<>();
        trigramCounts = new HashMap<>();
        tetragramCounts = new HashMap<>();
        pentagramCounts = new HashMap<>();
        unigramPerplexities = new HashMap<>();
        bigramPerplexities = new HashMap<>();
        trigramPerplexities = new HashMap<>();
        tetragramPerplexities = new HashMap<>();
        pentagramPerplexities = new HashMap<>();
        interpolatedPerplexities = new HashMap<>();
    }

    public void execute() {
        // Load trained ngram counts
        int n = 1;
        for (String gramFilename : gramResultsFilenames) {
            try {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(new FileInputStream(gramFilename), NLPCalculatorConstants.TR_CHARSET));

                Map<String,Integer> gramCounts = getNGramCounts(n);

                String key = reader.readLine();
                String value = reader.readLine();

                while (key != null && value != null) {
                    gramCounts.put(key,Integer.parseInt(value));
                    key = reader.readLine();
                    value = reader.readLine();
                }

                reader.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            ++n;
        }

        // Save total character for unigram probabilities
        for (String key : unigramCounts.keySet()) {
            totalCharacter += unigramCounts.get(key);
        }
    }

    // ----------- Used functions to train system and get results --------------
    private void loadTrainingData() {
        for (String path : trainingSetPaths) {
            File directory = new File(path);

            for (File file : directory.listFiles()) {
                try {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(new FileInputStream(file), NLPCalculatorConstants.TR_CHARSET));

                    String line = reader.readLine();

                    while (line != null) {
                        calculateNGrams(line);
                        line = reader.readLine();
                    }

                    reader.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void loadTestData() {
        try {
            File directory = new File(NLPCalculatorConstants.TEST_NEWS_PATH);

            for (File file : directory.listFiles()) {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(new FileInputStream(file), NLPCalculatorConstants.TR_CHARSET));

                String line = reader.readLine();

                while (line != null) {
                    if (!line.trim().isEmpty() && line.length() != 0) {
                        calculateNGramPerplexity(line);
                        calculatePerplexityWithInterpolation(line,
                                                             PENTAGRAM_LAMBDA,
                                                             TETRAGRAM_LAMBDA,
                                                             TRIGRAM_LAMBDA,
                                                             BIGRAM_LAMBDA,
                                                             UNIGRAM_LAMBDA);
                    }
                    line = reader.readLine();
                }

                reader.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void saveGramCountsResults() {
        int n = 1;

        // Saving N-Grams Counts...
        for (String filename : gramResultsFilenames) {
            try {
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(new FileOutputStream(filename), NLPCalculatorConstants.TR_CHARSET));
                Map<String,Integer> gramResult =  getNGramCounts(n);

                for (String key : gramResult.keySet()) {
                    writer.write(key + "\n");
                    writer.write(gramResult.get(key)+"\n");
                }

                writer.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            ++n;
        }
    }

    private void saveGramPerplexityResults() {
        // Saving N-Grams Perplexities...
        try {
            BufferedWriter writer = new BufferedWriter((
                    new OutputStreamWriter(new FileOutputStream(NLPCalculatorConstants.PERPLEXITY_RESULTS_FILE), NLPCalculatorConstants.TR_CHARSET)));

            for (String key : unigramPerplexities.keySet()) {
                writer.write("-------------------------------------------------------------\n");
                writer.write("Sentence:\n");


                for (int i = 0; i < key.length(); ++i) {
                    if (i % 80 == 0) {
                        writer.write("\n");
                    }
                    writer.write(key.charAt(i));
                }

                writer.write("\n\n******************** STANDARD PERPLEXITY ***********************\n");
                writer.write("Unigram Perplexity = " + unigramPerplexities.get(key) + "\n");
                writer.write("Bigram Perplexity =  " + bigramPerplexities.get(key) + "\n");
                writer.write("Trigram Perplexity =  " + trigramPerplexities.get(key) + "\n");
                writer.write("Tetragram Perplexity =  " + tetragramPerplexities.get(key) + "\n");
                writer.write("Pentagram Perplexity =  " + pentagramPerplexities.get(key) + "\n\n");
                writer.write("***************** PERPLEXITY WITH INTERPOLATION ****************\n");
                writer.write("Lambda 1 (Pentagram) = " + PENTAGRAM_LAMBDA + "\n");
                writer.write("Lambda 2 (Tetragram) = " + TETRAGRAM_LAMBDA + "\n");
                writer.write("Lambda 3 (Trigram) = " + TRIGRAM_LAMBDA + "\n");
                writer.write("Lambda 4 (Bigram) = " + BIGRAM_LAMBDA + "\n");
                writer.write("Lambda 5 (Unigram) = " + UNIGRAM_LAMBDA + "\n");
                writer.write("Perplexity = " + interpolatedPerplexities.get(key) + "\n");
                writer.write("-------------------------------------------------------------\n\n");
            }

            writer.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    // -------------------------------------------------------------------------

    // ------------------------------ PART 1 -----------------------------------
    public Map<String,Integer> getNGramCounts(int n) {
        switch (n) {
            case NLPCalculatorConstants.UNIGRAM:
                return unigramCounts;

            case NLPCalculatorConstants.BIGRAM:
                return bigramCounts;

            case NLPCalculatorConstants.TRIGRAM:
                return trigramCounts;

            case NLPCalculatorConstants.TETRAGRAM:
                return tetragramCounts;

            case NLPCalculatorConstants.PENTAGRAM:
                return pentagramCounts;

            default:
                return null;
        }
    }

    private void calculateNGrams(String line) {
        for (int i = 0; i < line.length(); i++) {
           calculateNGramsHelper(line,i,1);
           calculateNGramsHelper(line,i,2);
           calculateNGramsHelper(line,i,3);
           calculateNGramsHelper(line,i,4);
           calculateNGramsHelper(line,i,5);
        }
    }

    private void calculateNGramsHelper(String line, int index, int n) {
        if (index + n < line.length()) {
            String key = line.substring(index,index + n);
            Map<String,Integer> gram = getNGramCounts(n);
            if (gram.keySet().contains(key)) {
                gram.put(key, gram.get(key) + 1);
            } else {
                gram.put(key,1);
            }
        }
    }
    // -------------------------------------------------------------------------

    // ----------------------------- PART 2 ------------------------------------
    private double calculateProbability(String... token) {
        switch (token.length) {
            case NLPCalculatorConstants.UNIGRAM:
                return calculateUnigramProbability(token[0]);
            case NLPCalculatorConstants.BIGRAM:
                return calculateBigramProbability(token[0],token[1]);
            case NLPCalculatorConstants.TRIGRAM:
                return calculateTrigramProbability(token[0],token[1],token[2]);
            case NLPCalculatorConstants.TETRAGRAM:
                return calculateTetragramProbability(token[0],token[1],token[2],token[3]);
            case NLPCalculatorConstants.PENTAGRAM:
                return calculatePentagramProbability(token[0],token[1],token[2],token[3],token[4]);
            default:
                return 0;
        }
    }

    private double calculateUnigramProbability(String token) {
        if (unigramCounts.containsKey(token)) {
            return (double)unigramCounts.get(token) / (double)totalCharacter;
        } else {
            return 0;
        }
    }

    private double calculateBigramProbability(String... tokens) {
        String bigramKey = tokens[0] + tokens[1];
        String unigramKey = tokens[0];

        if (bigramCounts.containsKey(bigramKey) && unigramCounts.containsKey(unigramKey)) {
            return (double)bigramCounts.get(bigramKey) / (double)unigramCounts.get(unigramKey);
        } else {
            return 0;
        }
    }

    private double calculateTrigramProbability(String... tokens) {
        String trigramKey = tokens[0] + tokens[1] + tokens[2];
        String bigramKey = tokens[0] + tokens[1];

        if (trigramCounts.containsKey(trigramKey) && bigramCounts.containsKey(bigramKey)) {
            return (double)trigramCounts.get(trigramKey) / (double)bigramCounts.get(bigramKey);
        } else {
            return 0;
        }
    }

    private double calculateTetragramProbability(String... tokens) {
        String tetragramKey = tokens[0] + tokens[1] + tokens[2] + tokens[3];
        String trigramKey = tokens[0] + tokens[1] + tokens[2];

        if (tetragramCounts.containsKey(tetragramKey) && trigramCounts.containsKey(trigramKey)) {
            return (double)tetragramCounts.get(tetragramKey) / (double)trigramCounts.get(trigramKey);
        } else {
            return 0;
        }
    }

    private double calculatePentagramProbability(String... tokens) {
        String pentagramKey = tokens[0] + tokens[1] + tokens[2] + tokens[3] + tokens[4];
        String tetragramKey = tokens[0] + tokens[1] + tokens[2] + tokens[3];

        if (pentagramCounts.containsKey(pentagramKey) && tetragramCounts.containsKey(tetragramKey)) {
            return (double)pentagramCounts.get(pentagramKey) / (double)tetragramCounts.get(tetragramKey);
        } else {
            return 0;
        }
    }

    private void calculateNGramPerplexity(String sentence) {
        unigramPerplexities.put(sentence,calculateUnigramPerplexity(sentence));
        bigramPerplexities.put(sentence,calculateBigramPerplexity(sentence));
        trigramPerplexities.put(sentence,calculateTrigramPerplexity(sentence));
        tetragramPerplexities.put(sentence,calculateTetragramPerplexity(sentence));
        pentagramPerplexities.put(sentence,calculatePentagramPerplexity(sentence));
    }

    private double calculateUnigramPerplexity(String sentence) {
        double unigramPerplexity = 1;

        for (int i = 0; i < sentence.length(); ++i) {
            if (i + 1 < sentence.length()) {
                unigramPerplexity *= 1.0 / calculateProbability(sentence.substring(i, i + 1));
            }
        }

        unigramPerplexity = Math.pow(unigramPerplexity,1.0/(double)sentence.length());

        return unigramPerplexity;
    }

    private double calculateBigramPerplexity(String sentence) {
        double bigramPerplexity = 1;

        for (int i = 0; i < sentence.length(); ++i) {
            if (i + 1 < sentence.length() && i + 2 < sentence.length()) {
                bigramPerplexity *= 1.0 / calculateProbability(sentence.substring(i, i + 1),
                                                               sentence.substring(i + 1, i + 2));
            }
        }

        bigramPerplexity = Math.pow(bigramPerplexity,1.0/(double)sentence.length());

        return bigramPerplexity;
    }

    private double calculateTrigramPerplexity(String sentence) {
        double trigramPerplexity = 1;

        for (int i = 0; i < sentence.length(); ++i) {
            if (i + 1 < sentence.length() && i + 2 < sentence.length() &&
                i + 3 < sentence.length()) {
                trigramPerplexity *= 1.0 / calculateProbability(sentence.substring(i, i + 1),
                                                                sentence.substring(i + 1, i + 2),
                                                                sentence.substring(i + 2, i + 3));
            }
        }

        trigramPerplexity = Math.pow(trigramPerplexity,1.0/(double)sentence.length());

        return trigramPerplexity;
    }

    private double calculateTetragramPerplexity(String sentence) {
        double tetragramPerplexity = 1;

        for (int i = 0; i < sentence.length(); ++i) {
            if (i + 1 < sentence.length() && i + 2 < sentence.length() &&
                i + 3 < sentence.length() && i + 4 < sentence.length()) {
                tetragramPerplexity *= 1.0 / calculateProbability(sentence.substring(i, i + 1),
                                                                  sentence.substring(i + 1, i + 2),
                                                                  sentence.substring(i + 2, i + 3),
                                                                  sentence.substring(i + 3, i + 4));
            }
        }

        tetragramPerplexity = Math.pow(tetragramPerplexity,1.0/(double)sentence.length());

        return tetragramPerplexity;
    }

    private double calculatePentagramPerplexity(String sentence) {
        double pentagramPerplexity = 1;

        for (int i = 0; i < sentence.length(); ++i) {
            if (i + 1 < sentence.length() && i + 2 < sentence.length() &&
                i + 3 < sentence.length() && i + 4 < sentence.length() &&
                i + 5 < sentence.length()) {
                pentagramPerplexity *= 1.0 / calculateProbability(sentence.substring(i, i + 1),
                                                                  sentence.substring(i + 1, i + 2),
                                                                  sentence.substring(i + 2, i + 3),
                                                                  sentence.substring(i + 3, i + 4),
                                                                  sentence.substring(i + 4, i + 5));
            }
        }

        pentagramPerplexity = Math.pow(pentagramPerplexity,1.0/(double)sentence.length());

        return pentagramPerplexity;
    }
    // -------------------------------------------------------------------------

    // ---------------------------- PART 3 -------------------------------------
    // Calculate perplexity with interpolation for Pentagram
    public void calculatePerplexityWithInterpolation(String sentence, double... lambdas) {
        double perplexityResult = 1;
        double sumLambda = lambdas[0] + lambdas[1] + lambdas[2] + lambdas[3] + lambdas[4];


        if (sumLambda == 1.0) {
            for (int i = 0; i < sentence.length(); ++i) {
                if (i + 1 < sentence.length() && i + 2 < sentence.length() &&
                    i + 3 < sentence.length() && i + 4 < sentence.length() &&
                    i + 5 < sentence.length()){
                    perplexityResult *= 1 / (interpolation(i, NLPCalculatorConstants.PENTAGRAM, sentence, lambdas));
                }
            }
        }

        perplexityResult = Math.pow(perplexityResult,1.0/(double)sentence.length());

        interpolatedPerplexities.put(sentence,perplexityResult);
    }

    // ---------------------- INTERPOLATION -----------------------------------
    // L1 + L2 + L3 + L4 + L5 = 1.0
    //
    // Example:
    // P^(W5|W1,W2,W3,W4):
    // (L1 * P(W5|W1,W2,W3,W4)) + (L2 * P(W5|W2,W3,W4)) + (L3 * P(W5|W3,W4)) +
    // (L4 * P(W5|W4)) + (L5 * P(W5))
    //
    // -------------------------------------------------------------------------
    private double interpolation(int index, int n, String sentence, double... lambdas) {
        String[] tokens = new String[5];

        double unigramProb,
               bigramProb,
               trigramProb,
               tetragramProb,
               pentagramProb;

        switch (n) {
            case NLPCalculatorConstants.UNIGRAM:
                tokens[0] = sentence.substring(index,index+1);

                unigramProb = calculateProbability(tokens[0]);

                return lambdas[0] * unigramProb;

            case NLPCalculatorConstants.BIGRAM:
                tokens[0] = sentence.substring(index,index+1);
                tokens[1] = sentence.substring(index+1,index+2);

                unigramProb = calculateProbability(tokens[1]);
                bigramProb = calculateProbability(tokens[0],tokens[1]);

                return lambdas[0] * bigramProb + lambdas[1] * unigramProb;

            case NLPCalculatorConstants.TRIGRAM:
                tokens[0] = sentence.substring(index,index+1);
                tokens[1] = sentence.substring(index+1,index+2);
                tokens[2] = sentence.substring(index+2,index+3);

                unigramProb = calculateProbability(tokens[2]);
                bigramProb = calculateProbability(tokens[1],tokens[2]);
                trigramProb = calculateProbability(tokens[0],tokens[1],tokens[2]);

                return lambdas[0] * trigramProb + lambdas[1] * bigramProb +
                       lambdas[2] * unigramProb;

            case NLPCalculatorConstants.TETRAGRAM:
                tokens[0] = sentence.substring(index,index+1);
                tokens[1] = sentence.substring(index+1,index+2);
                tokens[2] = sentence.substring(index+2,index+3);
                tokens[3] = sentence.substring(index+3,index+4);

                unigramProb = calculateProbability(tokens[3]);
                bigramProb = calculateProbability(tokens[2],tokens[3]);
                trigramProb = calculateProbability(tokens[1],tokens[2],tokens[3]);
                tetragramProb = calculateProbability(tokens[0],tokens[1],tokens[2],tokens[3]);

                return lambdas[0] * tetragramProb + lambdas[1] * trigramProb +
                       lambdas[2] * bigramProb + lambdas[3] * unigramProb;

            case NLPCalculatorConstants.PENTAGRAM:
                tokens[0] = sentence.substring(index,index+1);
                tokens[1] = sentence.substring(index+1,index+2);
                tokens[2] = sentence.substring(index+2,index+3);
                tokens[3] = sentence.substring(index+3,index+4);
                tokens[4] = sentence.substring(index+4,index+5);

                unigramProb = calculateProbability(tokens[4]);
                bigramProb = calculateProbability(tokens[3],tokens[4]);
                trigramProb = calculateProbability(tokens[2],tokens[3],tokens[4]);
                tetragramProb = calculateProbability(tokens[1],tokens[2],tokens[3],tokens[4]);
                pentagramProb = calculateProbability(tokens[0],tokens[1],tokens[2],tokens[3],tokens[4]);

                return  lambdas[0] * pentagramProb + lambdas[1] * tetragramProb +
                        lambdas[2] * trigramProb + lambdas[3] * bigramProb +
                        lambdas[4] * unigramProb;
            default:
                return 0;
        }
    }
    // -------------------------------------------------------------------------

    // ------------------------------ PART 4 -----------------------------------
    public double assignProbabilityToSentence(String sentence, int n, boolean interpolationEnabled) {
        double probability = 1;

        for (int i = 0; i < sentence.length(); i++) {
            switch (n) {
                case NLPCalculatorConstants.UNIGRAM:
                    if (i + 1 < sentence.length()) {
                        if (interpolationEnabled) {
                            probability *= interpolation(i,
                                                         NLPCalculatorConstants.UNIGRAM,
                                                         sentence,
                                                         UNIGRAM_LAMBDA);
                        } else {
                            probability *= calculateProbability(sentence.substring(i, i + 1));
                        }
                    }
                    break;

                case NLPCalculatorConstants.BIGRAM:
                    if (i + 1 < sentence.length() && i + 2 < sentence.length()) {
                        if (interpolationEnabled) {
                            probability *= interpolation(i,
                                                         NLPCalculatorConstants.BIGRAM,
                                                         sentence,
                                                         BIGRAM_LAMBDA,
                                                         UNIGRAM_LAMBDA);
                        } else {
                            probability *= calculateProbability(sentence.substring(i,i+1),
                                                                sentence.substring(i+1,i+2));                        }

                    }
                    break;

                case NLPCalculatorConstants.TRIGRAM:
                    if (i + 1 < sentence.length() && i + 2 < sentence.length() &&
                        i + 3 < sentence.length()) {
                        if (interpolationEnabled) {
                            probability *= interpolation(i,
                                                         NLPCalculatorConstants.TRIGRAM,
                                                         sentence,
                                                         TRIGRAM_LAMBDA,
                                                         BIGRAM_LAMBDA,
                                                         UNIGRAM_LAMBDA);
                        } else {
                            probability *= calculateProbability(sentence.substring(i,i+1),
                                                                sentence.substring(i+1,i+2),
                                                                sentence.substring(i+2,i+3));
                        }

                    }
                    break;

                case NLPCalculatorConstants.TETRAGRAM:
                    if (i + 1 < sentence.length() && i + 2 < sentence.length() &&
                        i + 3 < sentence.length() && i + 4 < sentence.length()) {
                        if (interpolationEnabled) {
                            probability *= interpolation(i,
                                                         NLPCalculatorConstants.TETRAGRAM,
                                                         sentence,
                                                         TETRAGRAM_LAMBDA,
                                                         TRIGRAM_LAMBDA,
                                                         BIGRAM_LAMBDA,
                                                         UNIGRAM_LAMBDA);
                        } else {
                            probability *= calculateProbability(sentence.substring(i,i+1),
                                                                sentence.substring(i+1,i+2),
                                                                sentence.substring(i+2,i+3),
                                                                sentence.substring(i+3,i+4));
                        }
                    }
                    break;

                case NLPCalculatorConstants.PENTAGRAM:
                    if (i + 1 < sentence.length() && i + 2 < sentence.length() &&
                        i + 3 < sentence.length() && i + 4 < sentence.length() &&
                        i + 5 < sentence.length()) {
                        if (interpolationEnabled) {
                            probability *= interpolation(i,
                                                         NLPCalculatorConstants.PENTAGRAM,
                                                         sentence,
                                                         PENTAGRAM_LAMBDA,
                                                         TETRAGRAM_LAMBDA,
                                                         TRIGRAM_LAMBDA,
                                                         BIGRAM_LAMBDA,
                                                         UNIGRAM_LAMBDA);
                        } else {
                            probability *= calculateProbability(sentence.substring(i,i+1),
                                                                sentence.substring(i+1,i+2),
                                                                sentence.substring(i+2,i+3),
                                                                sentence.substring(i+3,i+4),
                                                                sentence.substring(i+4,i+5));
                        }
                    }
                    break;
            }
        }

        return probability;
    }
    // -------------------------------------------------------------------------
}
