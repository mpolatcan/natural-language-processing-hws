import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by mpolatcan-gyte_cse on 22.05.2017.
 */
public class CategoryVector {
    private String categoryName;
    private ArrayList<String> categoryVocabulary;
    private ArrayList<Double> categoryTfIdfValues;
    private HashMap<String,Integer> categoryDocumentFrequencies;

    public CategoryVector(String categoryName) {
        categoryVocabulary = new ArrayList<>();
        categoryTfIdfValues = new ArrayList<>();
        categoryDocumentFrequencies = new HashMap<>();
        this.categoryName = categoryName;
    }

    public ArrayList<Double> getCategoryTfIdfValues() {
        return categoryTfIdfValues;
    }

    public void addWordSetToVocabulary(Iterator documentWordSetIter) {
        while (documentWordSetIter.hasNext()) {
            String documentWord = ((String) documentWordSetIter.next());
            if (!categoryVocabulary.contains(documentWord)) {
                categoryVocabulary.add(documentWord);
            }
        }
    }

    public void calculateCategoryTfIdfValues(String word, int totalDocumentsNum,
                                     HashMap<String,Integer> baseVocabularyDocumentFrequencies) {
        Integer categoryWordCount = categoryDocumentFrequencies.get(word);
        int mostPassedWordNum = findMostPassedWordNum(categoryDocumentFrequencies);

        if (categoryWordCount != null) {
            double termFreq = (double) categoryWordCount / mostPassedWordNum;
            double inverseDocumentFreq = Math.log((double)totalDocumentsNum / baseVocabularyDocumentFrequencies.get(word));
            categoryTfIdfValues.add(termFreq * inverseDocumentFreq);
        } else {
            categoryTfIdfValues.add(0.0);
        }
    }

    public void calculateCategoryDocumentFrequencies(Set<String> documentWordSet) {
        for (String categoryVocabularyWord : categoryVocabulary) {
            if (documentWordSet.contains(categoryVocabularyWord)) {
                if (categoryDocumentFrequencies.containsKey(categoryVocabularyWord)) {
                    categoryDocumentFrequencies.put(categoryVocabularyWord,
                            categoryDocumentFrequencies.get(categoryVocabularyWord) + 1);
                } else {
                    categoryDocumentFrequencies.put(categoryVocabularyWord, 1);
                }
            }
        }
    }

    private int findMostPassedWordNum(HashMap<String,Integer> categoryDocumentFrequency) {
        int maxValue = 0;

        for (Integer value : categoryDocumentFrequency.values()) {
            if (maxValue < value) {
                maxValue = value;
            }
        }

        return maxValue;
    }
}
