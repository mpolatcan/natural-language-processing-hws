import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by mpolatcan-gyte_cse on 22.05.2017.
 */
public class BaseClassifierSystem {
    private String[] categories = { NLPConstants.CATEGORY_ECONOMY,
                                    NLPConstants.CATEGORY_HEALTH,
                                    NLPConstants.CATEGORY_MAGAZINE,
                                    NLPConstants.CATEGORY_POLITICAL,
                                    NLPConstants.CATEGORY_SPORTS};
    private ArrayList<DocumentVector> documentVectors;
    private ArrayList<CategoryVector> categoryVectors;
    private ArrayList<String> vocabulary;
    private HashMap<String, Integer> vocabularyDocumentFrequencies;
    private int totalDocumentsNum;

    public BaseClassifierSystem() {
        documentVectors = new ArrayList<>();
        categoryVectors = new ArrayList<>();
        vocabulary = new ArrayList<>();
        vocabularyDocumentFrequencies = new HashMap<>();

        for (int i = 0; i < 5; i++) {
            categoryVectors.add(new CategoryVector(categories[i]));
        }

        loadTrainingDocuments();
        createVocabulary();
        calculateDocumentFrequencies();
        calculateCategoryTfIdfValues();
        calculateDocumentTfIdfValues();
    }

    public ArrayList<DocumentVector> getDocumentVectors() {
        return documentVectors;
    }

    public ArrayList<CategoryVector> getCategoryVectors() {
        return categoryVectors;
    }

    public String[] getCategories() {
        return categories;
    }

    public int getTotalDocumentsNum() {
        return totalDocumentsNum;
    }

    public ArrayList<String> getVocabulary() {
        return vocabulary;
    }

    public HashMap<String, Integer> getVocabularyDocumentFrequencies() {
        return vocabularyDocumentFrequencies;
    }

    private void loadTrainingDocuments() {
        System.out.println("Load training documents...");

        File economyDocuments = new File(NLPConstants.ECONOMY);
        File healthDocuments = new File(NLPConstants.HEALTH);
        File magazineDocuments = new File(NLPConstants.MAGAZINE);
        File politicalDocuments = new File(NLPConstants.POLITICAL);
        File sportsDocuments = new File(NLPConstants.SPORTS);

        /* ------------------ Read documents and save.. ----------------------*/
        for (File economyDocument : economyDocuments.listFiles()) {
            documentVectors.add(new DocumentVector(economyDocument,NLPConstants.CATEGORY_ECONOMY));
        }

        for (File healthDocument : healthDocuments.listFiles()) {
            documentVectors.add(new DocumentVector(healthDocument,NLPConstants.CATEGORY_HEALTH));
        }

        for (File magazineDocument : magazineDocuments.listFiles()) {
            documentVectors.add(new DocumentVector(magazineDocument,NLPConstants.CATEGORY_MAGAZINE));
        }

        for (File politicalDocument : politicalDocuments.listFiles()) {
            documentVectors.add(new DocumentVector(politicalDocument,NLPConstants.CATEGORY_POLITICAL));
        }

        for (File sportsDocument : sportsDocuments.listFiles()) {
            documentVectors.add(new DocumentVector(sportsDocument,NLPConstants.CATEGORY_SPORTS));
        }
        /* -------------------------------------------------------------------*/

        totalDocumentsNum = documentVectors.size(); // Save total documents number
    }

    private void createVocabulary() {
        System.out.println("Creating base vocabulary...");

        /* -------------  Create base vocabulary of system... ----------------*/
        for (DocumentVector documentVector : documentVectors) {
            Iterator documentWordSetIter = documentVector.getVocabulary().keySet().iterator();

            while (documentWordSetIter.hasNext()) {
                String documentWord = ((String) documentWordSetIter.next());
                if (!vocabulary.contains(documentWord)) {
                    vocabulary.add(documentWord);
                }
            }
        }
        /* -------------------------------------------------------------------*/

        System.out.println("Creating vocabularies of categories...");

        /* ----------- Create vocabularies of categories... ------------------*/
        for (DocumentVector documentVector : documentVectors) {
            Iterator documentWordSetIter = documentVector.getVocabulary().keySet().iterator();

            switch (documentVector.getCategory()) {
                case NLPConstants.CATEGORY_ECONOMY:
                    categoryVectors.get(0).addWordSetToVocabulary(documentWordSetIter);
                    break;

                case NLPConstants.CATEGORY_HEALTH:
                    categoryVectors.get(1).addWordSetToVocabulary(documentWordSetIter);
                    break;

                case NLPConstants.CATEGORY_MAGAZINE:
                    categoryVectors.get(2).addWordSetToVocabulary(documentWordSetIter);
                    break;

                case NLPConstants.CATEGORY_POLITICAL:
                    categoryVectors.get(3).addWordSetToVocabulary(documentWordSetIter);
                    break;

                case NLPConstants.CATEGORY_SPORTS:
                    categoryVectors.get(4).addWordSetToVocabulary(documentWordSetIter);
                    break;
            }
        }
        /* -------------------------------------------------------------------*/
    }

    private void calculateDocumentFrequencies() {
        System.out.println("Calculate base vocabulary's document frequencies...");

        /* -------- Calculate base vocabulary's document frequencies ---------*/
        for (DocumentVector documentVector : documentVectors) {
            Set<String> documentWordSet = documentVector.getVocabulary().keySet();

            for (String vocabularyWord : vocabulary) {
                if (documentWordSet.contains(vocabularyWord)) {
                    if (vocabularyDocumentFrequencies.containsKey(vocabularyWord)) {
                        vocabularyDocumentFrequencies.put(vocabularyWord,
                                vocabularyDocumentFrequencies.get(vocabularyWord) + 1);
                    } else {
                        vocabularyDocumentFrequencies.put(vocabularyWord, 1);
                    }
                }
            }
        }
        /* ------------------------------------------------------------------- */

        System.out.println("Calculate vocabularies of categories' document frequencies...");
        for (DocumentVector documentVector : documentVectors) {
            Set<String> documentWordSet = documentVector.getVocabulary().keySet();

            switch (documentVector.getCategory()) {
                case NLPConstants.CATEGORY_ECONOMY:
                    categoryVectors.get(0).calculateCategoryDocumentFrequencies(documentWordSet);
                    break;

                case NLPConstants.CATEGORY_HEALTH:
                    categoryVectors.get(1).calculateCategoryDocumentFrequencies(documentWordSet);
                    break;

                case NLPConstants.CATEGORY_MAGAZINE:
                    categoryVectors.get(2).calculateCategoryDocumentFrequencies(documentWordSet);
                    break;

                case NLPConstants.CATEGORY_POLITICAL:
                    categoryVectors.get(3).calculateCategoryDocumentFrequencies(documentWordSet);
                    break;

                case NLPConstants.CATEGORY_SPORTS:
                    categoryVectors.get(4).calculateCategoryDocumentFrequencies(documentWordSet);
                    break;
            }
        }
    }

    private void calculateCategoryTfIdfValues() {
        System.out.println("Calculating tf-idf vectors of categories...");
        for (String word : vocabulary) {
            categoryVectors.get(0).calculateCategoryTfIdfValues(word,totalDocumentsNum,vocabularyDocumentFrequencies);
            categoryVectors.get(1).calculateCategoryTfIdfValues(word,totalDocumentsNum,vocabularyDocumentFrequencies);
            categoryVectors.get(2).calculateCategoryTfIdfValues(word,totalDocumentsNum,vocabularyDocumentFrequencies);
            categoryVectors.get(3).calculateCategoryTfIdfValues(word,totalDocumentsNum,vocabularyDocumentFrequencies);
            categoryVectors.get(4).calculateCategoryTfIdfValues(word,totalDocumentsNum,vocabularyDocumentFrequencies);
        }
    }

    private void calculateDocumentTfIdfValues() {
        System.out.println("Calculating document tf-idf vectors...");
        for (DocumentVector documentVector : documentVectors) {
            documentVector.calculateDocumentTfIdfValues(vocabulary,totalDocumentsNum,vocabularyDocumentFrequencies);
        }
    }

    public double calculateSimilarity(ArrayList<Double> testDocumentTfIdfValues,
                                       ArrayList<Double> otherTfIdfValues) {
        double magnitudeTestDocumentTfIdf = 0;
        double magnitudeOtherTfIdf = 0;
        double mult = 0;
        double result;

        for (Double value : testDocumentTfIdfValues) {
            magnitudeTestDocumentTfIdf += Math.pow(value,2);
        }

        magnitudeTestDocumentTfIdf = Math.sqrt(magnitudeTestDocumentTfIdf);

        for (Double value : otherTfIdfValues) {
            magnitudeOtherTfIdf += Math.pow(value,2);
        }

        magnitudeOtherTfIdf = Math.sqrt(magnitudeOtherTfIdf);


        for (int i = 0; i < testDocumentTfIdfValues.size(); ++i) {
            mult += testDocumentTfIdfValues.get(i) * otherTfIdfValues.get(i);
        }

        result = (mult) / (magnitudeTestDocumentTfIdf * magnitudeOtherTfIdf);
        result = Math.toDegrees(Math.acos(result));

        return result;
    }

    public static int findMostPassedWordNum(HashMap<String,Integer> categoryDocumentFrequency) {
        int maxValue = 0;

        for (Integer value : categoryDocumentFrequency.values()) {
            if (maxValue < value) {
                maxValue = value;
            }
        }

        return maxValue;
    }
}
