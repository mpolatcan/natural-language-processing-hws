import java.io.File;
import java.util.ArrayList;

/**
 * Created by mpolatcan-gyte_cse on 11.05.2017.
 */
public class RocchioClassifier implements Classifier {
    private BaseClassifierSystem baseClassifierSystem;
    private int correctPredictNum = 0;

    public RocchioClassifier() {
        baseClassifierSystem = new BaseClassifierSystem();
    }

    public int getCorrectPredictNum() {
        return correctPredictNum;
    }

    public void clearCorrectPredictNum() {
        correctPredictNum = 0;
    }

    public String[] execute(String filename, String actualCategory) {
        String[] results = new String[2];
        DocumentVector testDocumentVector = new DocumentVector(new File(filename),null);
        testDocumentVector.calculateDocumentTfIdfValues(baseClassifierSystem.getVocabulary(),
                                                        baseClassifierSystem.getTotalDocumentsNum(),
                                                        baseClassifierSystem.getVocabularyDocumentFrequencies());

        ArrayList<CategoryVector> categoryVectors = baseClassifierSystem.getCategoryVectors();

        double[] similarities = new double[5];

        similarities[0] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentTfIdfValues(),
                                                                    categoryVectors.get(0).getCategoryTfIdfValues());
        similarities[1] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentTfIdfValues(),
                                                                   categoryVectors.get(1).getCategoryTfIdfValues());
        similarities[2] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentTfIdfValues(),
                                                                   categoryVectors.get(2).getCategoryTfIdfValues());
        similarities[3] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentTfIdfValues(),
                                                                   categoryVectors.get(3).getCategoryTfIdfValues());
        similarities[4] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentTfIdfValues(),
                                                                   categoryVectors.get(4).getCategoryTfIdfValues());

        double minAngle = Double.MAX_VALUE;
        int assignedClass = 0;

        for (int i = 0; i < 5; i++) {
            if (similarities[i] < minAngle) {
                minAngle = similarities[i];
                assignedClass = i;
            }
        }

        results[0] = baseClassifierSystem.getCategories()[assignedClass];
        results[1] = String.valueOf(minAngle);

        if (results[0].equals(actualCategory)) {
            ++correctPredictNum;
        }

        return results;
    }




}
