import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Created by mpolatcan-gyte_cse on 11.05.2017.
 */
public class DocumentVector {
    private HashMap<String, Integer> documentVocabulary;
    private ArrayList<Double> documentTfIdfValues;
    private String category;

    public DocumentVector(File document, String category) {
        documentVocabulary = new HashMap<>();
        documentTfIdfValues = new ArrayList<>();
        this.category = category;

        readDocument(document);
    }

    public ArrayList<Double> getDocumentTfIdfValues() {
        return documentTfIdfValues;
    }

    public HashMap<String,Integer> getVocabulary() {
        return documentVocabulary;
    }

    public String getCategory() {
        return category;
    }

    private void readDocument(File document) {
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(document), NLPConstants.TR_CHARSET));

            String line = reader.readLine();

            while (line != null) {
                line.replaceAll("[\\t\\n\\r]+"," ");
                StringTokenizer tokenizer = new StringTokenizer(line);

                while (tokenizer.hasMoreTokens()) {
                    String token = tokenizer.nextToken();
                    String key;

                    if (token.length() > 5) {
                        key = token.substring(0, 5);
                    } else {
                        key = token;
                    }

                    if (documentVocabulary.keySet().contains(key)) {
                        documentVocabulary.put(key, documentVocabulary.get(key) + 1);
                    } else {
                        documentVocabulary.put(key, 1);
                    }
                }

                line = reader.readLine();
            }

            reader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void calculateDocumentTfIdfValues(ArrayList<String> vocabulary, int totalDocumentsNum,
                                            HashMap<String,Integer> vocabularyDocumentFrequencies) {
        for (String word : vocabulary) {
            Integer wordCount = documentVocabulary.get(word);
            int mostPassedWordNum = BaseClassifierSystem.findMostPassedWordNum(documentVocabulary);

            if (wordCount != null) {
                double termFreq = (double) wordCount / mostPassedWordNum;
                double inverseDocumentFreq =
                        Math.log((double)totalDocumentsNum / vocabularyDocumentFrequencies.get(word));
                documentTfIdfValues.add(termFreq * inverseDocumentFreq);
            } else {
                documentTfIdfValues.add(0.0);
            }
        }
    }
}
