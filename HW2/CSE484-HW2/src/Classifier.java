/**
 * Created by mpolatcan-gyte_cse on 23.05.2017.
 */
public interface Classifier {
    String[] execute(String filename, String actualCategory);
    int getCorrectPredictNum();
}
