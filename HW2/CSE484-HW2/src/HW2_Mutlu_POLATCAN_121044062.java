import java.io.File;
import java.util.Scanner;

/**
 * Created by mpolatcan-gyte_cse on 11.05.2017.
 */
public class HW2_Mutlu_POLATCAN_121044062 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Select the method (1-Rocchio 2-K-Nearest Neighbor): ");
        int classificationMethod = scanner.nextInt();

        Classifier classifier = null;

        if (classificationMethod == 1) {
            classifier = new RocchioClassifier();
        } else if (classificationMethod == 2){
            System.out.print("Enter k size: " );
            scanner.nextLine();
            classifier = new KNNClassifier(scanner.nextInt());
        }

        File economyTestDirectory = new File(NLPConstants.TEST_NEWS_PATH + "/economy/");
        File politicalTestDirectory = new File(NLPConstants.TEST_NEWS_PATH + "/political/");
        File sportsTestDirectory = new File(NLPConstants.TEST_NEWS_PATH + "/sports/");
        File magazineTestDirectory = new File(NLPConstants.TEST_NEWS_PATH + "/magazine/");
        File healthTestDirectory = new File(NLPConstants.TEST_NEWS_PATH + "/health/");

        int totalTestDocuments = economyTestDirectory.listFiles().length +
                                 politicalTestDirectory.listFiles().length +
                                 sportsTestDirectory.listFiles().length +
                                 magazineTestDirectory.listFiles().length +
                                 healthTestDirectory.listFiles().length;

        for (File economyTestDocument : economyTestDirectory.listFiles()) {
            String[] results = classifier.execute(economyTestDocument.getPath(), NLPConstants.CATEGORY_ECONOMY);
            System.out.print("File name: " + economyTestDocument.getName() + ", Actual class: " + NLPConstants.CATEGORY_ECONOMY);
            System.out.println(", Assigned class " + results[0] + ", similarity score is " + results[1]);
        }

        for (File politicalTestDocument : politicalTestDirectory.listFiles()) {
            String[] results = classifier.execute(politicalTestDocument.getPath(), NLPConstants.CATEGORY_POLITICAL);
            System.out.print("File name: " + politicalTestDocument.getName() + ", Actual class: " + NLPConstants.CATEGORY_POLITICAL);
            System.out.println(", Assigned class " + results[0] + ", similarity score is " + results[1]);
        }

        for (File magazineTestDocument : magazineTestDirectory.listFiles()) {
            String[] results = classifier.execute(magazineTestDocument.getPath(), NLPConstants.CATEGORY_MAGAZINE);
            System.out.print("File name: " + magazineTestDocument.getName() + ", Actual class: " + NLPConstants.CATEGORY_MAGAZINE);
            System.out.println(", Assigned class " + results[0] + ", similarity score is " + results[1]);
        }

        for (File healthTestDocument : healthTestDirectory.listFiles()) {
            String[] results = classifier.execute(healthTestDocument.getPath(), NLPConstants.CATEGORY_HEALTH);
            System.out.print("File name: " + healthTestDocument.getName() + ", Actual class: " + NLPConstants.CATEGORY_HEALTH);
            System.out.println(", Assigned class " + results[0] + ", similarity score is " + results[1]);
        }

        for (File sportsTestDocument : sportsTestDirectory.listFiles()) {
            String[] results = classifier.execute(sportsTestDocument.getPath(), NLPConstants.CATEGORY_SPORTS);
            System.out.print("File name: " + sportsTestDocument.getName() + ", Actual class: " + NLPConstants.CATEGORY_SPORTS);
            System.out.println(", Assigned class " + results[0] + ", similarity score is " + results[1]);
        }

        System.out.println("Accuracy: " + 100 * ((double)classifier.getCorrectPredictNum()/totalTestDocuments));
    }
}
