import java.nio.charset.Charset;

/**
 * Created by mpolatcan-gyte_cse on 09.04.2017.
 */
public interface NLPConstants {
    String TEST_NEWS_PATH = "test news";
    String NEWS_PATH = "news";
    String ECONOMY = NEWS_PATH + "/economy";
    String HEALTH = NEWS_PATH + "/health";
    String MAGAZINE = NEWS_PATH + "/magazine";
    String POLITICAL = NEWS_PATH + "/political";
    String SPORTS = NEWS_PATH + "/sports";
    Charset TR_CHARSET = Charset.forName("ISO-8859-9");
    String CATEGORY_ECONOMY = "ECONOMY";
    String CATEGORY_HEALTH = "HEALTH";
    String CATEGORY_MAGAZINE = "MAGAZINE";
    String CATEGORY_POLITICAL = "POLITICAL";
    String CATEGORY_SPORTS = "SPORTS";
}
