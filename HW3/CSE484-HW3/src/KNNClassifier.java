import java.io.File;
import java.util.ArrayList;

/**
 * Created by mpolatcan-gyte_cse on 22.05.2017.
 */
public class KNNClassifier implements Classifier {
    private BaseClassifierSystem baseClassifierSystem;
    private int k;
    private int correctPredictNum = 0;

    public KNNClassifier(int k) {
        this.k = k;
        baseClassifierSystem = new BaseClassifierSystem();
    }

    @Override
    public int getCorrectPredictNum() {
        return correctPredictNum;
    }

    public String[] execute(String filename, String actualCategory, int method) {
        DocumentVector documentVector = new DocumentVector(new File(filename),
                                                           actualCategory,
                                                           baseClassifierSystem.getWord2VecData(),
                                                           false);
        return calculateSimilarity(documentVector.getDocumentVector(method),actualCategory,method);
    }

    public String[] calculateSimilarity(ArrayList<Double> testDocumentVector, String actualCategory, int method) {
        String[] results = new String[2];
        ArrayList<DocumentVector> documentVectors = baseClassifierSystem.getDocumentVectors();
        Neighbor[] kNearestNeighbors = new Neighbor[k];
        int index = 0;

        for (DocumentVector documentVector : documentVectors) {
            double distance = baseClassifierSystem.calculateSimilarity(testDocumentVector,documentVector.getDocumentVector(method));

            if (index < k) {
                kNearestNeighbors[index] = new Neighbor(documentVector,distance);
                ++index;
            } else {
                ArrayList<Integer> farNeighbors = new ArrayList<>();

                for (int i = 0; i < k; i++) {
                    if (distance < kNearestNeighbors[i].getDistance()) {
                        farNeighbors.add(i);
                    }
                }

                double maxDistance = 0;
                int maxDistanceNeighborIndex = 0;

                for (int i = 0; i < farNeighbors.size(); ++i) {
                    if (kNearestNeighbors[farNeighbors.get(i)].getDistance() > maxDistance) {
                        maxDistance = kNearestNeighbors[farNeighbors.get(i)].getDistance();
                        maxDistanceNeighborIndex = farNeighbors.get(i);
                    }
                }

                if (farNeighbors.size() != 0) {
                    kNearestNeighbors[maxDistanceNeighborIndex] = new Neighbor(documentVector, distance);
                }
            }
        }

        int[] categoryCounters = new int[5];

        for (int i = 0; i < k; i++) {
            switch (kNearestNeighbors[i].getDocumentVector().getCategory()) {
                case NLPConstants.CATEGORY_ECONOMY:
                    ++categoryCounters[0];
                    break;
                case NLPConstants.CATEGORY_MAGAZINE:
                    ++categoryCounters[1];
                    break;
                case NLPConstants.CATEGORY_SPORTS:
                    ++categoryCounters[2];
                    break;
                case NLPConstants.CATEGORY_HEALTH:
                    ++categoryCounters[3];
                    break;
                case NLPConstants.CATEGORY_POLITICAL:
                    ++categoryCounters[4];
                    break;
            }
        }

        int maxCount = 0;
        int maxCountIndex = 0;

        for (int i = 0; i < 5; i++) {
            if (categoryCounters[i] > maxCount) {
                maxCount = categoryCounters[i];
                maxCountIndex = i;
            }
        }

        double minDistance = Double.MAX_VALUE;

        for (int i = 0; i < kNearestNeighbors.length; ++i) {
            if (kNearestNeighbors[i].getDistance() < minDistance) {
                minDistance = kNearestNeighbors[i].getDistance();
            }
        }

        results[0] = baseClassifierSystem.getCategories()[maxCountIndex];
        results[1] = String.valueOf(minDistance);

        if (results[0].equals(actualCategory)) {
            ++correctPredictNum;
        }

        return results;
    }
}
