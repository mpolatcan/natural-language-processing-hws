/**
 * Created by mpolatcan-gyte_cse on 22.05.2017.
 */
public class Neighbor {
    private DocumentVector documentVector;
    private double distance;

    public Neighbor(DocumentVector documentVector, double distance) {
        this.documentVector = documentVector;
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }

    public DocumentVector getDocumentVector() {
        return documentVector;
    }

    @Override
    public String toString() {
        return String.format("%.2f ", distance);
    }
}
