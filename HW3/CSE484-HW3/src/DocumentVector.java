import java.io.*;
import java.util.*;

/**
 * Created by mpolatcan-gyte_cse on 11.05.2017.
 */
public class DocumentVector {
    private ArrayList<Double> minVector;
    private ArrayList<Double> maxVector;
    private ArrayList<Double> avgVector;
    private ArrayList<Double> zeroVector;
    private ArrayList<ArrayList<Double>> wordVectors;
    private String category;

    public DocumentVector(File document, String category) {
        minVector = new ArrayList<>();
        maxVector = new ArrayList<>();
        avgVector = new ArrayList<>();
        zeroVector = new ArrayList<>(Collections.nCopies(100,0.0));
        wordVectors = new ArrayList<>();
        this.category = category;

        loadDocumentVector(document.getName());
        prepareAvgVector();
        prepareMaxVector();
        prepareMinVector();
    }

    public DocumentVector(File document,
                          String category,
                          HashMap<String,ArrayList<Double>> word2VecData,
                          boolean trainDocument) {
        minVector = new ArrayList<>();
        maxVector = new ArrayList<>();
        avgVector = new ArrayList<>();
        zeroVector = new ArrayList<>(Collections.nCopies(100,0.0));
        wordVectors = new ArrayList<>();
        this.category = category;

        readDocument(document,word2VecData);

        if (trainDocument) {
            saveDocumentVector(document.getName());
        }

        prepareAvgVector();
        prepareMaxVector();
        prepareMinVector();
    }

    public ArrayList<Double> getDocumentVector(int method) {
        switch (method) {
            case NLPConstants.METHOD_AVG:
                return avgVector;
            case NLPConstants.METHOD_MAX:
                return maxVector;
            case NLPConstants.METHOD_MIN:
                return minVector;
            default:
                return null;
        }
    }

    public void printWordVectors() {
        for (ArrayList<Double> wordVector : wordVectors) {
            System.out.println(wordVector);
        }
    }

    public String getCategory() {
        return category;
    }

    private void readDocument(File document, HashMap<String,ArrayList<Double>> word2VecData) {
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(document), NLPConstants.TR_CHARSET));

            String line = reader.readLine();

            while (line != null) {
                line.replaceAll("[\\t\\n\\r]+"," ");
                String[] tokens = line.split(" ");

                for (int i = 0; i < tokens.length; i++) {
                    ArrayList<Double> wordVector;

                    if (tokens[i].length() > 5) {
                        wordVector = word2VecData.get(tokens[i].toLowerCase().substring(0, 5));
                    } else {
                        wordVector = word2VecData.get(tokens[i].toLowerCase());
                    }

                    if (wordVector != null) {
                        wordVectors.add(wordVector);
                    } else {
                        wordVectors.add(zeroVector);
                    }
                }

                line = reader.readLine();
            }

            reader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void saveDocumentVector(String filename) {
        String[] tokens = filename.split("\\.");

        try {
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(NLPConstants.TRAIN_DOCUMENT_VECTORS +
                            tokens[0] + " vectors.txt")));

            for (ArrayList<Double> wordVector : wordVectors) {
                String wordVectorStr = wordVector.toString().replace("[","").replace(",","").replace("]","");
                writer.write(wordVectorStr);
                writer.write("\n");
            }

            writer.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void loadDocumentVector(String filename) {
        String[] tokens = filename.split("\\.");

        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(NLPConstants.TRAIN_DOCUMENT_VECTORS +
                        tokens[0] + " vectors.txt")));

            String line = reader.readLine();

            while (line != null) {
                line.replaceAll("[\\t\\n\\r]+"," ");
                StringTokenizer tokenizer = new StringTokenizer(line);

                ArrayList<Double> wordVector = new ArrayList<>();

                while (tokenizer.hasMoreTokens()) {
                    wordVector.add(Double.parseDouble(tokenizer.nextToken()));
                }

                wordVectors.add(wordVector);

                line = reader.readLine();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void prepareAvgVector() {
        int wordNum = wordVectors.size();
        int vectorSize = wordVectors.get(0).size();

        for (int i = 0; i < vectorSize; ++i) {
            double avg = 0;

            for (ArrayList<Double> wordVector : wordVectors) {
                avg += wordVector.get(i);
            }

            avg /= wordNum;

            avgVector.add(avg);
        }
    }

    private void prepareMinVector() {
        int vectorSize = wordVectors.get(0).size();

        for (int i = 0; i < vectorSize; ++i) {
            ArrayList<Double> bucket = new ArrayList<>();

            for (ArrayList<Double> wordVector : wordVectors) {
                bucket.add(wordVector.get(i));
            }

            bucket.sort(Comparator.naturalOrder());

            minVector.add(bucket.get(0));
        }
    }

    private void prepareMaxVector() {
        int vectorSize = wordVectors.get(0).size();

        for (int i = 0; i < vectorSize; ++i) {
            ArrayList<Double> bucket = new ArrayList<>();

            for (ArrayList<Double> wordVector : wordVectors) {
                bucket.add(wordVector.get(i));
            }

            bucket.sort(Comparator.naturalOrder());

            maxVector.add(bucket.get(bucket.size() - 1));
        }
    }
}
