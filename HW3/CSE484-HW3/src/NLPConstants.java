import java.nio.charset.Charset;

/**
 * Created by mpolatcan-gyte_cse on 09.04.2017.
 */
public interface NLPConstants {
    String TEST_NEWS_PATH = "test news";
    String TRAIN_DOCUMENT_VECTORS = "train document vectors/";
    String TR_WIKI_VECTORS = "trwikivectors.txt";
    String NEWS_PATH = "news";
    String ECONOMY = NEWS_PATH + "/economy";
    String HEALTH = NEWS_PATH + "/health";
    String MAGAZINE = NEWS_PATH + "/magazine";
    String POLITICAL = NEWS_PATH + "/political";
    String SPORTS = NEWS_PATH + "/sports";
    Charset TR_CHARSET = Charset.forName("ISO-8859-9");
    String CATEGORY_ECONOMY = "ECONOMY";
    String CATEGORY_HEALTH = "HEALTH";
    String CATEGORY_MAGAZINE = "MAGAZINE";
    String CATEGORY_POLITICAL = "POLITICAL";
    String CATEGORY_SPORTS = "SPORTS";
    int METHOD_AVG = 1;
    int METHOD_MAX = 2;
    int METHOD_MIN = 3;
    int TR_WIKI_VOCABULARY_SIZE = 453224;
    int TRAIN_DOCUMENT_NUM = 1090;
}
