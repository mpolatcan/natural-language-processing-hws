import javax.print.Doc;
import java.util.*;

/**
 * Created by mpolatcan-gyte_cse on 22.05.2017.
 */
public class CategoryVector {
    private ArrayList<Double> minVector;
    private ArrayList<Double> maxVector;
    private ArrayList<Double> avgVector;
    private ArrayList<DocumentVector> documentVectors;

    public CategoryVector() {
        minVector = new ArrayList<>();
        maxVector = new ArrayList<>();
        avgVector = new ArrayList<>();
        documentVectors = new ArrayList<>();
    }

    public void addDocumentVector(DocumentVector documentVector) {
        documentVectors.add(documentVector);
    }

    public void prepareMinMaxAvgVectors() {
        prepareAvgVector();
        prepareMaxVector();
        prepareMinVector();
    }

    public ArrayList<Double> getCategoryVectory(int method) {
       switch (method) {
           case NLPConstants.METHOD_AVG:
               return avgVector;
           case NLPConstants.METHOD_MAX:
               return maxVector;
           case NLPConstants.METHOD_MIN:
               return minVector;
           default:
               return null;
       }
    }

    private void prepareAvgVector() {
        int documentNum = documentVectors.size();
        int vectorSize = documentVectors.get(0).getDocumentVector(NLPConstants.METHOD_AVG).size();

        for (int i = 0; i < vectorSize; i++) {
            double avg = 0;

            for (DocumentVector documentVector : documentVectors) {
                avg += documentVector.getDocumentVector(NLPConstants.METHOD_AVG).get(i);
            }

            avg /= documentNum;

            avgVector.add(avg);
        }
    }

    private void prepareMinVector() {
        int vectorSize = documentVectors.get(0).getDocumentVector(NLPConstants.METHOD_MIN).size();

        for (int i = 0; i < vectorSize; i++) {
            ArrayList<Double> bucket = new ArrayList<>();

            for (DocumentVector documentVector : documentVectors) {
               bucket.add(documentVector.getDocumentVector(NLPConstants.METHOD_MIN).get(i));
            }

            bucket.sort(Comparator.naturalOrder());

            minVector.add(bucket.get(0));
        }
    }

    private void prepareMaxVector() {
        int vectorSize = documentVectors.get(0).getDocumentVector(NLPConstants.METHOD_MAX).size();

        for (int i = 0; i < vectorSize; i++) {
            ArrayList<Double> bucket = new ArrayList<>();

            for (DocumentVector documentVector : documentVectors) {
                bucket.add(documentVector.getDocumentVector(NLPConstants.METHOD_MAX).get(i));
            }

            bucket.sort(Comparator.naturalOrder());

            maxVector.add(bucket.get(bucket.size()-1));
        }
    }
}
