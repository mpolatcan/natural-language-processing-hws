import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mpolatcan-gyte_cse on 11.05.2017.
 */
public class RocchioClassifier implements Classifier {
    private BaseClassifierSystem baseClassifierSystem;
    private int correctPredictNum = 0;

    public RocchioClassifier() {
        baseClassifierSystem = new BaseClassifierSystem();
    }

    public int getCorrectPredictNum() {
        return correctPredictNum;
    }

    public String[] execute(String filename, String actualCategory, int method) {
        String[] results = new String[2];
        DocumentVector testDocumentVector =
            new DocumentVector(new File(filename),actualCategory,baseClassifierSystem.getWord2VecData(),false);

        HashMap<String,CategoryVector> categoryVectors = baseClassifierSystem.getCategoryVectors();

        double[] similarities = new double[5];

        similarities[0] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentVector(method),
                  categoryVectors.get(NLPConstants.CATEGORY_ECONOMY).getCategoryVectory(method));
        similarities[1] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentVector(method),
                  categoryVectors.get(NLPConstants.CATEGORY_MAGAZINE).getCategoryVectory(method));
        similarities[2] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentVector(method),
                  categoryVectors.get(NLPConstants.CATEGORY_SPORTS).getCategoryVectory(method));
        similarities[3] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentVector(method),
                  categoryVectors.get(NLPConstants.CATEGORY_HEALTH).getCategoryVectory(method));
        similarities[4] = baseClassifierSystem.calculateSimilarity(testDocumentVector.getDocumentVector(method),
                  categoryVectors.get(NLPConstants.CATEGORY_POLITICAL).getCategoryVectory(method));

        double minAngle = Double.MAX_VALUE;
        int assignedClass = 0;

        for (int i = 0; i < 5; i++) {
            if (similarities[i] < minAngle) {
                minAngle = similarities[i];
                assignedClass = i;
            }
        }

        results[0] = baseClassifierSystem.getCategories()[assignedClass];
        results[1] = String.valueOf(minAngle);

        if (results[0].equals(actualCategory)) {
            ++correctPredictNum;
        }

        return results;
    }
}
