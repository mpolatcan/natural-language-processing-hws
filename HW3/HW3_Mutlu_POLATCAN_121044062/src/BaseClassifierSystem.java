import java.io.*;
import java.util.*;

/**
 * Created by mpolatcan-gyte_cse on 22.05.2017.
 */
public class BaseClassifierSystem {
    private String[] categories = { NLPConstants.CATEGORY_ECONOMY,
                                    NLPConstants.CATEGORY_MAGAZINE,
                                    NLPConstants.CATEGORY_SPORTS,
                                    NLPConstants.CATEGORY_HEALTH,
                                    NLPConstants.CATEGORY_POLITICAL};
    private ArrayList<DocumentVector> documentVectors;
    private HashMap<String,CategoryVector> categoryVectors;
    private HashMap<String,ArrayList<Double>> word2VecData;
    private boolean vectorsPrepared = false;

    public BaseClassifierSystem() {
        documentVectors = new ArrayList<>();
        word2VecData = new HashMap<>();
        categoryVectors = new HashMap<>();

        for (int i = 0; i < categories.length; i++) {
            categoryVectors.put(categories[i],new CategoryVector());
        }

        File trainDocumentVectorsDirectory = new File(NLPConstants.TRAIN_DOCUMENT_VECTORS);

        if (trainDocumentVectorsDirectory.listFiles().length == NLPConstants.TRAIN_DOCUMENT_NUM) {
            vectorsPrepared = true;
        }

        loadWord2VecData();
        loadDocumentVectors();
        loadCategoryVectors();
    }

    private void loadWord2VecData() {
        try {
            int loadedWordVectorNum = 0;

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(NLPConstants.TR_WIKI_VECTORS)));

            String line;

            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split(" ");

                if (tokens[0].length() > 5) {
                    tokens[0] = tokens[0].substring(0,5);
                }

                word2VecData.put(tokens[0].toLowerCase(),new ArrayList<>());

                ArrayList<Double> wordVector = word2VecData.get(tokens[0].toLowerCase());

                for (int i = 1; i < tokens.length; i++) {
                    wordVector.add(Double.parseDouble(tokens[i]));
                }

                System.out.print("\rWord2Vec datas loading: %" +
                    String.format("%.2f",100 * ((double) loadedWordVectorNum / NLPConstants.TR_WIKI_VOCABULARY_SIZE)));

                ++loadedWordVectorNum;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.out.println();
    }

    private void loadDocumentVectors() {
        int loadedDocumentVectorNum = 0;

        if (vectorsPrepared) {
            System.out.println("Train document vectors are already prepared!");
        }

        File economyNewsDirectory = new File(NLPConstants.ECONOMY);
        File sportsNewsDirectory = new File(NLPConstants.SPORTS);
        File magazineNewsDirectory = new File(NLPConstants.MAGAZINE);
        File politicalNewsDirectory = new File(NLPConstants.POLITICAL);
        File healthNewsDirectory = new File(NLPConstants.HEALTH);

        for (File document : economyNewsDirectory.listFiles()) {
            if (vectorsPrepared) {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_ECONOMY));
            } else {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_ECONOMY,word2VecData,true));
            }
            ++loadedDocumentVectorNum;
            System.out.print("\rTrain document vectors are loading: %" + String.format("%.2f",
                    100 * ((double) loadedDocumentVectorNum / NLPConstants.TRAIN_DOCUMENT_NUM)));
        }

        for (File document : sportsNewsDirectory.listFiles()) {
            if (vectorsPrepared) {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_SPORTS));
            } else {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_SPORTS,word2VecData,true));
            }

            ++loadedDocumentVectorNum;
            System.out.print("\rTrain document vectors are loading: %" + String.format("%.2f",
                    100 * ((double) loadedDocumentVectorNum / NLPConstants.TRAIN_DOCUMENT_NUM)));
        }

        for (File document : magazineNewsDirectory.listFiles()) {
            if (vectorsPrepared) {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_MAGAZINE));
            } else {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_MAGAZINE,word2VecData,true));
            }

            ++loadedDocumentVectorNum;
            System.out.print("\rTrain document vectors are loading: %" + String.format("%.2f",
                    100 * ((double) loadedDocumentVectorNum / NLPConstants.TRAIN_DOCUMENT_NUM)));
        }

        for (File document : politicalNewsDirectory.listFiles()) {
            if (vectorsPrepared) {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_POLITICAL));
            } else {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_POLITICAL,word2VecData,true));
            }

            ++loadedDocumentVectorNum;
            System.out.print("\rTrain document vectors are loading: %" + String.format("%.2f",
                    100 * ((double) loadedDocumentVectorNum / NLPConstants.TRAIN_DOCUMENT_NUM)));
        }

        for (File document : healthNewsDirectory.listFiles()) {
            if (vectorsPrepared) {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_HEALTH));
            } else {
                documentVectors.add(new DocumentVector(document, NLPConstants.CATEGORY_HEALTH,word2VecData,true));
            }

            ++loadedDocumentVectorNum;
            System.out.print("\rTrain document vectors are loading: %" + String.format("%.2f",
                    100 * ((double) loadedDocumentVectorNum / NLPConstants.TRAIN_DOCUMENT_NUM)));
        }

        System.out.println("\nVectors are loaded!");
    }

    private void loadCategoryVectors() {
        for (DocumentVector documentVector : documentVectors) {
            categoryVectors.get(documentVector.getCategory()).addDocumentVector(documentVector);
        }

        for (String category : categoryVectors.keySet()) {
            categoryVectors.get(category).prepareMinMaxAvgVectors();
        }
    }

    public HashMap<String, CategoryVector> getCategoryVectors() {
        return categoryVectors;
    }

    public HashMap<String, ArrayList<Double>> getWord2VecData() {
        return word2VecData;
    }

    public ArrayList<DocumentVector> getDocumentVectors() {
        return documentVectors;
    }

    public String[] getCategories() {
        return categories;
    }

    public double calculateSimilarity(ArrayList<Double> testDocumentVector,
                                      ArrayList<Double> otherVector) {
        double magnitudeTestDocumentVector = 0;
        double magnitudeOtherVector = 0;
        double mult = 0;
        double result;

        for (Double value : testDocumentVector) {
            magnitudeTestDocumentVector += Math.pow(value,2);
        }

        magnitudeTestDocumentVector = Math.sqrt(magnitudeTestDocumentVector);

        for (Double value : otherVector) {
            magnitudeOtherVector += Math.pow(value,2);
        }

        magnitudeOtherVector = Math.sqrt(magnitudeOtherVector);


        for (int i = 0; i < testDocumentVector.size(); ++i) {
            mult += testDocumentVector.get(i) * otherVector.get(i);
        }

        result = (mult) / (magnitudeTestDocumentVector * magnitudeOtherVector);
        result = Math.toDegrees(Math.acos(result));

        return result;
    }
}
